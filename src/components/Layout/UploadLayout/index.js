import Header from "../DefaultLayout/Header";
function UploadLayout({ children }) {
    return (
        <div>
            <Header />
            <div>{children}</div>
        </div>
    );
}

export default UploadLayout;
