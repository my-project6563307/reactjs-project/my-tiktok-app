import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import "./App.css";
import { publicRoutes } from "./routes";
import { DefaultLayout } from "./components/Layout";
function App() {
    return (
        <Router>
            <div className="app">
                <Link to="/">Home</Link>
                <Link to="/following">Folow</Link>
                <Link to="/profile">Profile</Link>
                <Link to="/upload">Upload</Link>
                <Routes>
                    {publicRoutes.map((route, index) => {
                        const Page = route.component;
                        const Layout = route.layout ?? DefaultLayout;
                        return (
                            <Route
                                key={index}
                                path={route.path}
                                element={
                                    <Layout>
                                        <Page />
                                    </Layout>
                                }
                            />
                        );
                    })}
                </Routes>
            </div>
        </Router>
    );
}

export default App;
